--[[
	libhttp | HTTP API Wrapper
	        | Pantheon Project (galaxiorg/pantheon)

	Filename
	  main.lua
	Author
	  daelvn (https://gitlab.com/daelvn)
	Version
		libconf-d2
	License
		MIT License (LICENSE.md)
	Documentation
	  manual* libhttp:about
		* Pantheon/manual : Manual viewer
	Description
		libhttp is a wrapper around the native HTTP API that supports GET, POST, PUT
		and DELETE (only the two first are native) with an easy way of handling
		headers and parameters.
]]--

-- As it is OO, use an object
local libhttp = {}
libhttp.HttpConnection = {}
HttpConnectionMT = getmetatable(libhttp.HttpConnection)
HttpConnectionMT.__index = libhttp.HttpConnection

-- Create a new instance of HttpConnection
function libhttp.HttpConnection.new(url)
	local self = setmetatable({}, HttpConnection)
	self.url = url
	self.parameters = {}
	self.headers = {}
end

function libhttp.HttpConnection:appendUrl(append)
	self.url = self.url .. append
end

-- Add a parameter
function libhttp.HttpConnection:addParameter(name, value)
	if (not name) or (not value) then return false end
	self.parameters[tostring(name)] = textutils.urlEncode(tostring(value))
	return true
end

-- Remove a parameter
function libhttp.HttpConnection:removeParameter(name)
	if (not name) then return false end
	self.parameters[tostring(name)] = nil
	return true
end

-- Encode parameters for GET request
function libhttp.HttpConnection:encodeParameters()
	local append = "?"
	for name, value in pairs(self.parameters) do
		append = append .. name .. "=" .. value .. "&"
	end
	return append
end

-- Add header
function libhttp.HttpConnection:addHeader(name, value)
	if (not name) or (not value) then return false end
	self.headers[tostring(name)] = textutils.urlEncode(tostring(value))
	return true
end

-- Remove header
function libhttp.HttpConnection:removeHeader(name)
	if (not name) then return false end
	self.headers[tostring(name)] = nil
	return true
end

-- Send GET request (async)
function libhttp.HttpConnection:getAsync()
	http.request(self.url .. self:encodeParameters(), {}, self.headers)
end

-- Send POST request (async)
function libhttp.HttpConnection:postAsync()
	http.request(self.url, self.parameters, self.headers)
end

-- Send GET request (sync)
function libhttp.HttpConnection:get()
	return http.get(self.url .. self:encodeParameters(), self.headers)
end

-- Send POST request (sync)
function libhttp.HttpConnection:post()
	return http.post(self.url, self.parameters, self.headers)
end

-- Send PUT request (sync)
function libhttp.HttpConnection:put()
	self:addHeader("X-HTTP-Method-Override", "PUT")
	return http.post(self.url, self.parameters, self.headers)
end

-- Send DELETE request (sync)
function libhttp.HttpConnection:delete()
	self:addHeader("X-HTTP-Method-Override", "DELETE")
	return http.post(self.url, self.parameters, self.headers)
end

return libhttp
