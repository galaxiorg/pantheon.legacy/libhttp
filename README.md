# libhttp
HTTP API Wrapper for CC/Lua
## Usage (Async POST request example)
```lua
-- Use dofile to load the library
local libhttp = dofile("path/to/libhttp.lua")
-- Create a new HttpConnection
local connection = libhttp.HttpConnection.new("http://example.com")
-- Add the parameters for POST
connection:addParameter("name", "Dael Vnaja")
connection:addParameter("city", "Stockholm")
connection:addParameter("country", "Sweden")
-- Add headers (such as the user agent)
connection:addHeader("User-Agent", "ComputerCraft/libhttp 1.79")
connection:addHeader("X-Custom", "Custom header")
-- Make an async POST request
connection:postAsync()
-- Wait for the event
local requesting = true
while requesting do
  local event, url, sourceText = os.pullEvent()
  if event == "http_success" then
    local response = sourceText.readAll()
    sourceText.close()
    return response
    requesting = false
  elseif event == "http_failure" then
    error("Server didn't respond.")
    requesting = false
  end
end
```
