# libhttp Documentation
## Loading
`local libhttp = dofile("path/to/libhttp.lua")`

## HttpConnection.new(string url) -> HttpConnection
Takes an URL to make requests to.
`local connection = libhttp.HttpConnection.new(url)`

## HttpConnection:addParameter(string name, string value) -> bool
Adds a parameter to the request.
`connection:addParameter("user", "daelvn")`

## HttpConnection:removeParameter(string name) -> bool
Removes a parameter from the request.
`connection:removeParameter("user")`

## HttpConnection:encodeParameters() -> string
Returns a string with the parameters encoded for a GET request (`?a=b&c=d&`).
`local getParameters = connection:encodeParameters()`

## HttpConnection:addHeader(string name, string value) -> bool
Adds a header to the request.
`connection:addHeader("User-Agent", "ComputerCraft/libhttp 1.79")`

## HttpConnection:removeHeader(string name) -> bool
Removes a header from the connection.
`connection:removeHeader("User-Agent")`

## HttpConnection:getAsync()
Makes an asynchroneous GET request with the current parameters and headers.
You need to wait for a http_success or http_failure event.
`connection:getAsync()`

## HttpConnection:postAsync()
Makes an asynchroneous POST request with the current parameters and headers.
You need to wait for a http_success or http_failure event.
`connection:postAsync()`

## HttpConnection:get() -> Http
Makes a GET request with the current parameters and headers.
Returns a standard HTTP API handle
`local getHandle = connection:get()`

## HttpConnection:post() -> Http
Makes a POST request with the current parameters and headers.
Returns a standard HTTP API handle
`local getHandle = connection:post()`

## HttpConnection:put() -> Http
Makes a PUT request with the current parameters and headers.
Returns a standard HTTP API handle
`local getHandle = connection:put()`

## HttpConnection:delete() -> Http
Makes a DELETE request with the current parameters and headers.
Returns a standard HTTP API handle
`local getHandle = connection:delete()`
